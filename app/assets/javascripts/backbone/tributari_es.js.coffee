#= require_self
#= require_tree ./templates
#= require_tree ./models
#= require_tree ./views
#= require_tree ./routers

window.TributariEs =
  Models: {}
  Collections: {}
  Routers: {}
  Views: {}