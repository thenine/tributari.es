FactoryGirl.define do
  factory :user do
    name     "Jason Bowman"
    email    "jason@nine.is"
    password "wordpass"
    password_confirmation "wordpass"
  end
end